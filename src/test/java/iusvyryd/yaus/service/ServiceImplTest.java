package iusvyryd.yaus.service;

import iusvyryd.yaus.exception.YausException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import iusvyryd.yaus.dao.ClickDao;
import iusvyryd.yaus.dao.UrlDao;
import iusvyryd.yaus.model.Click;
import iusvyryd.yaus.model.Statistics;
import iusvyryd.yaus.model.Url;
import iusvyryd.yaus.service.impl.ServiceImpl;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ServiceImpl.class)
public class ServiceImplTest {

    // mock
    private UrlDao urlDao;
    // mock
    private ClickDao clickDao;

    private ServiceImpl service;

    @Before
    public void setUp() throws Exception {
        urlDao = mock(UrlDao.class);
        clickDao = mock(ClickDao.class);

        service = PowerMockito.spy(new ServiceImpl());
        PowerMockito.doReturn(urlDao).when(service, "createUrlDao");
        PowerMockito.doReturn(clickDao).when(service, "createClickDao");
    }

    @Test
    public void testNewLongUrlSuccess() throws Exception {
        Url url = new Url("http://longurl.example.com");

        doThrow(NoResultException.class).when(urlDao).findByLongUrlHash(anyInt());
        when(urlDao.save(any(Url.class)))
                .thenAnswer(new Answer<Url>() {
                    @Override
                    public Url answer(InvocationOnMock invocationOnMock) throws Throwable {
                        Url url = (Url)invocationOnMock.getArguments()[0];
                        url.setId(1);
                        return url;
                    }
                });

        assertNull(url.getId());
        assertNull(url.getLongUrlHash());
        assertNull(url.getShortId());

        url = service.processNewLongUrl(url);

        assertNotNull(url.getId());
        assertTrue(url.getId() > 0);
        assertNotNull(url.getLongUrlHash());
        assertNotNull(url.getShortId());

        PowerMockito.verifyPrivate(service, times(1)).invoke("createUrlDao");
        verify(urlDao, times(1)).findByLongUrlHash(anyInt());
        verify(urlDao, times(1)).save(any(Url.class));
    }

    @Test
    public void testNewLongUrlExisted() throws Exception {
        Url url = new Url("http://longurl.example.com");
        Url existed = new Url("http://longurl.example.com", "qwerty");
        existed.setLongUrlHash(1000);

        doReturn(existed).when(urlDao).findByLongUrlHash(anyInt());

        assertNull(url.getId());
        assertNull(url.getLongUrlHash());
        assertNull(url.getShortId());

        Url ret = service.processNewLongUrl(url);

        assertNotNull(ret.getLongUrlHash());
        assertNotNull(ret.getShortId());
        assertEquals(existed.getLongUrlHash(), ret.getLongUrlHash());
        assertEquals(existed.getShortId(), ret.getShortId());

        PowerMockito.verifyPrivate(service, times(1)).invoke("createUrlDao");
        verify(urlDao, times(1)).findByLongUrlHash(anyInt());
        verify(urlDao, times(0)).save(any(Url.class));
    }

    @Test(expected = YausException.class)
    public void testNewLongUrlExceptionSize() throws Exception {
        Url url = new Url("http://longurl.example.com");
        doThrow(NonUniqueResultException.class).when(urlDao).findByLongUrlHash(anyInt());
        service.processNewLongUrl(url);
    }

    @Test
    public void testResolveLongSuccess() throws Exception {
        String shortId = "qwerty";
        Url existed = new Url("http://longurl.example.com", shortId);

        doReturn(existed).when(urlDao).findByShortId(anyString());

        Url resolved = service.resolveLongUrl(shortId);

        assertNotNull(resolved);
        assertEquals(existed.getLongUrl(), resolved.getLongUrl());
        assertEquals(shortId, resolved.getShortId());

        PowerMockito.verifyPrivate(service, times(1)).invoke("createUrlDao");
        verify(urlDao, times(1)).findByShortId(anyString());
    }

    @Test(expected = YausException.class)
    public void testResolveLongExceptionSize() throws YausException {
        String shortId = "qwerty";
        doThrow(NonUniqueResultException.class).when(urlDao).findByShortId(anyString());
        service.resolveLongUrl(shortId);
    }

    @Test(expected = YausException.class)
    public void testResolveLongExceptionNotFound() throws YausException {
        String shortId = "qwerty";
        doThrow(NoResultException.class).when(urlDao).findByShortId(anyString());
        service.resolveLongUrl(shortId);
    }

    @Test
    public void testNewClickSuccess() throws Exception {
        Click c = new Click();

        when(clickDao.save(any(Click.class)))
                .thenAnswer(new Answer<Click>() {
                    @Override
                    public Click answer(InvocationOnMock invocationOnMock) throws Throwable {
                        Click cl = (Click) invocationOnMock.getArguments()[0];
                        cl.setId(1);
                        return cl;
                    }
                });

        assertNull(c.getId());
        assertNull(c.getBrowser());
        assertNull(c.getReferer());
        assertNull(c.getCountry());
        assertNull(c.getPlatform());

        c = service.processNewClick(c, "198.45.78.1",
                "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0)", "http://www.example.com");

        assertNotNull(c.getId());
        assertTrue(c.getId() > 0);
        assertNull(c.getCountry()); // geoip exection
        assertNotNull(c.getReferer());
        assertNotNull(c.getBrowser());
        assertNotNull(c.getPlatform());

        PowerMockito.verifyPrivate(service, times(1)).invoke("createClickDao");
        verify(clickDao, times(1)).save(any(Click.class));
    }

    @Test(expected = YausException.class)
    public void testNewClickException() throws Exception {
        Click cl = new Click();
        doThrow(Exception.class).when(clickDao).save(any(Click.class));
        service.processNewClick(cl, null, null, null);
    }

    @Test
    public void testFullStatsSuccess() throws Exception {
        when(clickDao.totalStatisticsReport(any(Url.class)))
                .thenAnswer(new Answer<Statistics>() {
                    @Override
                    public Statistics answer(InvocationOnMock invocationOnMock) throws Throwable {
                        Statistics st = new Statistics((Url)invocationOnMock.getArguments()[0]);
                        return st;
                    }
                });

        Statistics s = service.getFullStatistics(new Url());

        assertNotNull(s);
        PowerMockito.verifyPrivate(service, times(1)).invoke("createClickDao");
        verify(clickDao, times(1)).totalStatisticsReport(any(Url.class));
    }

    @Test(expected = YausException.class)
    public void testFullStatsException() throws Exception {
        doThrow(Exception.class).when(clickDao).totalStatisticsReport(any(Url.class));
        service.getFullStatistics(new Url());
    }

    @Test
    public void testGetAll() throws Exception {
        when(urlDao.getAll())
                .thenAnswer(new Answer<List<Url>>() {
                    @Override
                    public List<Url> answer(InvocationOnMock invocationOnMock) throws Throwable {
                        return new ArrayList<Url>();
                    }
                });

        List<Url> lu = service.getAllUrls();

        assertNotNull(lu);
        PowerMockito.verifyPrivate(service, times(1)).invoke("createUrlDao");
        verify(urlDao, times(1)).getAll();
    }

    @Test(expected = YausException.class)
    public void testGetAllException() throws Exception {
        doThrow(Exception.class).when(urlDao).getAll();
        service.getAllUrls();
    }

}
