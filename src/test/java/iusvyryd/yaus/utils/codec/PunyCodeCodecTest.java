package iusvyryd.yaus.utils.codec;

import org.junit.Before;
import org.junit.Test;
import iusvyryd.yaus.util.UrlCodec;
import iusvyryd.yaus.util.codec.PunyCodeCodec;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PunyCodeCodecTest {

    UrlCodec codec;

    @Before
    public void setUp() {
        codec = new PunyCodeCodec();
    }

    @Test
    public void testMalformedUrl() {
        String url = "qwerty";
        String encoded = codec.encode(url);
        assertTrue(encoded == null);
    }

    @Test
    public void testASCII() {
        String url = "http://example.com";
        String encoded = codec.encode(url);
        assertEquals(url, encoded);

        String decoded = codec.decode(encoded);
        assertEquals(url, decoded);
    }

    @Test
    public void testUnicode() {
        String url = "http://приклад.укр";
        String encoded = codec.encode(url);
        assertEquals("http://xn--80aikifvh.xn--j1amh", encoded);

        String decoded = codec.decode(encoded);
        assertEquals(url, decoded);
    }
}
