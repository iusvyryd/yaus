package iusvyryd.yaus.utils.codec;

import org.junit.Before;
import org.junit.Test;
import iusvyryd.yaus.util.HashCodec;
import iusvyryd.yaus.util.codec.Base62Codec;
import iusvyryd.yaus.util.hash.MurMur2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class Base62CodecTest {

    HashCodec codec;

    @Before
    public void setUp() {
        codec = new Base62Codec();
    }

    @Test
    public void testDigit() {
        int digit = 15;
        String encoded = codec.encode(digit);
        assertEquals("T", encoded);

        int decoded = codec.decode(encoded);
        assertEquals(digit, decoded);
    }

    @Test
    public void testNegativeDigit() {
        int digit = -15;
        String encoded = codec.encode(digit);
        assertTrue("_T".equals(encoded) || "T_".equals(encoded));

        int decoded = codec.decode(encoded);
        assertEquals(digit, decoded);
    }

    @Test
    public void testNumber() {
        int num = 36845579;
        String encoded = codec.encode(num);
        assertEquals("bNKUo", encoded);

        int decoded = codec.decode(encoded);
        assertEquals(num, decoded);
    }

    @Test
    public void testNegativeNumber() {
        int num = -36845579;
        String encoded = codec.encode(num);
        assertTrue(encoded.indexOf("_") != -1);
        assertEquals("bNKUo", encoded.replace("_", ""));

        int decoded = codec.decode(encoded);
        assertEquals(num, decoded);
    }

    @Test
    public void testHashEncDec() {
        String str = "Some text Some text Some text Some text";
        int hash = new MurMur2().hash(str.getBytes());

        String encoded = codec.encode(hash);
        assertTrue(encoded.length() <= 7);

        int decoded = codec.decode(encoded);
        assertEquals(hash, decoded);
    }

}
