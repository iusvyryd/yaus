package iusvyryd.yaus.utils.ua;

import org.junit.Test;
import iusvyryd.yaus.util.ua.UAParser;

import static org.junit.Assert.assertEquals;

public class UAParserTest {

    @Test
    public void testUAChromeWindows() {
        String ua = "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/40.0.2214.115 Safari/537.36";
        UAParser.UAInfo uainfo = UAParser.parse(ua);

        assertEquals("Chrome", uainfo.browser());
        assertEquals("Windows", uainfo.platform());
    }

    @Test
    public void testUAChromeLinux() {
        String ua = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu/11.10 Chrome/18.0.1025.168 Safari/535.19";
        UAParser.UAInfo uainfo = UAParser.parse(ua);

        assertEquals("Chrome", uainfo.browser());
        assertEquals("Linux", uainfo.platform());
    }

    @Test
     public void testUAFirefoxWindows() {
        String ua = "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0";
        UAParser.UAInfo uainfo = UAParser.parse(ua);

        assertEquals("Firefox", uainfo.browser());
        assertEquals("Windows", uainfo.platform());
    }

    @Test
    public void testUAFirefoxLinux() {
        String ua = "Mozilla/5.0 (X11; Linux x86_64; rv:35.0) Gecko/20100101 Firefox/35.0";
        UAParser.UAInfo uainfo = UAParser.parse(ua);

        assertEquals("Firefox", uainfo.browser());
        assertEquals("Linux", uainfo.platform());
    }

    @Test
    public void testUAIE() {
        String ua = "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko";
        UAParser.UAInfo uainfo = UAParser.parse(ua);

        assertEquals("MSIE", uainfo.browser());
        assertEquals("Windows", uainfo.platform());
    }

    @Test
    public void testAndroidBrowser() {
        String ua = "Mozilla/5.0 (Linux; U; Android 2.3.3; ko-kr; LG-LU3000 Build/GRI40) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1";
        UAParser.UAInfo uainfo = UAParser.parse(ua);

        assertEquals("Android Browser", uainfo.browser());
        assertEquals("Android", uainfo.platform());
    }

    @Test
    public void testIphoneSafari() {
        String ua = "Mozilla/5.0 (iPhone; U; ru; CPU iPhone OS 4_2_1 like Mac OS X; ru) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8C148a Safari/6533.18.5";
        UAParser.UAInfo uainfo = UAParser.parse(ua);

        assertEquals("Safari", uainfo.browser());
        assertEquals("iPhone", uainfo.platform());
    }

}
