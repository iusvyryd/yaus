package iusvyryd.yaus.utils.geoip;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import iusvyryd.yaus.util.geoip.GeoIP;

import static org.junit.Assert.assertTrue;

public class GeoIPTest {

    GeoIP geoip;

    @Before
    public void setUp() {
        geoip = new GeoIP();
    }

    @BeforeClass
    public static void openGeoIp() {
        GeoIP.open(GeoIPTest.class.getClassLoader().getResource("GeoLite2-Country.mmdb").getFile());
    }

    @AfterClass
    public static void closeGeoIp() {
        GeoIP.close();
    }

    @Test
    public void testLoopback() {
        String ip = "127.0.0.1";
        GeoIP.CountryInfo cinfo = geoip.resolveCountryInfo(ip);

        assertTrue(cinfo.name() == null);
        assertTrue(cinfo.iso() == null);
    }

    @Test
    public void testRealIp() {
        String ip = "91.198.36.14";
        GeoIP.CountryInfo cinfo = geoip.resolveCountryInfo(ip);

        assertTrue("Ukraine".equalsIgnoreCase(cinfo.name()));
        assertTrue("ua".equalsIgnoreCase(cinfo.iso()));
    }

}
