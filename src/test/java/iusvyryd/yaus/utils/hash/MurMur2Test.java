package iusvyryd.yaus.utils.hash;

import org.junit.Before;
import org.junit.Test;
import iusvyryd.yaus.util.HashAlgo;
import iusvyryd.yaus.util.hash.MurMur2;

import static org.junit.Assert.assertTrue;

public class MurMur2Test {

    HashAlgo algo;

    @Before
    public void setUp() {
       algo = new MurMur2();
    }

    @Test
    public void testEquals() {
        String str = "Some text Some text Some text Some text Some text Some text Some text Some text";
        int hash1 = algo.hash(str.getBytes());
        int hash2 = algo.hash(str.getBytes());

        assertTrue(hash1 == hash2);
    }

}
