package iusvyryd.yaus.dao.jpa;

import org.junit.*;
import iusvyryd.yaus.model.Url;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class JpaUrlDaoTest {

    private EntityManager em;
    private JpaUrlDao dao;
    private Url url;
    private Url url2;

    @Ignore
    @BeforeClass
    public static void setupEMF() {
        Emf.init("yausPU-test");
    }

    @AfterClass
    public static void closeEMF() {
        Emf.get().close();
    }

    @Before
    public void setUp() {
        em = Emf.get().createEntityManager();
        dao = new JpaUrlDao(em);

        url = new Url("http://example.com", "qwerty");
        url.setLongUrlHash(new Random().nextInt());
        url2 = new Url("http://example2.com", "qwerty2");
        url2.setLongUrlHash(new Random().nextInt());
    }

    @After
    public void tearDown() {
        dao.close();
    }

    @Test
    public void testSave() {
        dao.save(url);

        Url u = dao.findByLongUrlHash(url.getLongUrlHash());

        assertTrue(u != null);

        assertTrue(u.getId() != null);
        assertTrue(url.getLongUrlHash() == u.getLongUrlHash());
        assertEquals(url.getLongUrl(), u.getLongUrl());
        assertEquals(url.getShortId(), u.getShortId());

        // clear
        dao.remove(url);
    }

    @Test
    public void testFindByLongUrlHash() {
        dao.save(url);

        Url u = dao.findByLongUrlHash(url.getLongUrlHash());
        assertTrue(u != null);
        assertTrue(url.getLongUrlHash() == u.getLongUrlHash());
        assertEquals(url.getLongUrl(), u.getLongUrl());
        assertEquals(url.getShortId(), u.getShortId());

        dao.save(url2);

        u = dao.findByLongUrlHash(url2.getLongUrlHash());
        assertTrue(u != null);
        assertTrue(url2.getLongUrlHash() == u.getLongUrlHash());
        assertEquals(url2.getLongUrl(), u.getLongUrl());
        assertEquals(url2.getShortId(), u.getShortId());

        // clear
        dao.remove(url);
        dao.remove(url2);
    }

    @Test
    public void testFindByShortId() {
        dao.save(url);

        Url u = dao.findByShortId(url.getShortId());
        assertTrue(u != null);
        assertTrue(url.getLongUrlHash() == u.getLongUrlHash());
        assertEquals(url.getLongUrl(), u.getLongUrl());
        assertEquals(url.getShortId(), u.getShortId());

        dao.save(url2);

        u = dao.findByLongUrlHash(url2.getLongUrlHash());
        assertTrue(u != null);
        assertTrue(url2.getLongUrlHash() == u.getLongUrlHash());
        assertEquals(url2.getLongUrl(), u.getLongUrl());
        assertEquals(url2.getShortId(), u.getShortId());

        // clear
        dao.remove(url);
        dao.remove(url2);
    }

    @Test
    public void testGetAll() {
        dao.save(url);
        dao.save(url2);

        List<Url> uList = dao.getAll();

        assertTrue(uList.size() == 2);

        // clear
        dao.remove(url);
        dao.remove(url2);
    }

    @Test(expected = Exception.class)
    public void testFindByLongUrlHashException() {
        Url u = dao.findByLongUrlHash(url.getLongUrlHash());
    }

    @Test(expected = Exception.class)
    public void testFindByShortIdException() {
        Url u = dao.findByShortId(url.getShortId());
    }
}
