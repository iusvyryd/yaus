package iusvyryd.yaus.dao.jpa;

import org.junit.*;
import iusvyryd.yaus.model.Click;
import iusvyryd.yaus.model.Country;
import iusvyryd.yaus.model.Statistics;
import iusvyryd.yaus.model.Url;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertTrue;

public class JpaClickDaoTest {

    private EntityManager em;
    private JpaClickDao dao;
    private static Url url;
    private List<Click> clicks = new ArrayList<>();

    @BeforeClass
    public static void setupEMF() {
        Emf.init("yausPU-test");
    }

    @AfterClass
    public static void closeEMF() {
        Emf.get().close();
    }

    @Before
    public void setUp() {
        em = Emf.get().createEntityManager();
        dao = new JpaClickDao(em);

        url = new Url("http://example.com", "qwerty");
        url.setLongUrlHash(new Random().nextInt());

        try {
            em.getTransaction().begin();
            em.persist(url);
            em.getTransaction().commit();
        }
        catch (Exception e) {}
    }

    @After
    public void tearDown() {

        try {
            em.getTransaction().begin();
            for (Click click : clicks) {
                em.remove(click);
            }
            em.remove(url);
            em.getTransaction().commit();
        }
        catch (Exception e) { }
        clicks.clear();
        dao.close();
    }

    private Click createClick() {
        Click click = new Click(url);
        click.setBrowser("Browser");
        click.setReferer("www.example.com");
        click.setPlatform("Linux");
        click.setCountry(new Country("Ukraine", "ua"));
        clicks.add(click);
        return click;
    }

    @Test
    public void testSave() {
        Click click = createClick();

        assertTrue(click.getId() == null);

        click = dao.save(click);

        assertTrue(click != null);
        assertTrue(click.getId() != null);
    }

    @Test
    public void testTotalStatistics() {
        int cnt = 10;
        for (int i = 0; i < cnt; i++) {
            dao.save(createClick());
        }

        Click click = clicks.get(0);
        Statistics stat = dao.totalStatisticsReport(click.getUrl());

        assertTrue(stat != null);
        assertTrue(cnt == stat.getTotalCount());
        // by browser
        assertTrue(1 == stat.getGroupedByBrowser().size());
        assertTrue(null != stat.getGroupedByBrowser().get(click.getBrowser()));
        assertTrue(cnt == stat.getGroupedByBrowser().get(click.getBrowser()));
        // by referer
        assertTrue(1 == stat.getGroupedByReferer().size());
        assertTrue(1 == stat.getGroupedByReferer().size());
        assertTrue(null != stat.getGroupedByReferer().get(click.getReferer()));
        assertTrue(cnt == stat.getGroupedByReferer().get(click.getReferer()));
        // by platform
        assertTrue(1 == stat.getGroupedByPlatform().size());
        assertTrue(null != stat.getGroupedByPlatform().get(click.getPlatform()));
        assertTrue(cnt == stat.getGroupedByPlatform().get(click.getPlatform()));
        // by county
        assertTrue(1 == stat.getGroupedByCountry().size());
        assertTrue(null != stat.getGroupedByCountry().get(click.getCountry()));
        assertTrue(cnt == stat.getGroupedByCountry().get(click.getCountry()));
        // by date
        assertTrue(1 == stat.getGroupedByDate().size());
        assertTrue(cnt == stat.getGroupedByCountry().values().toArray(new Long[1])[0]);
    }

}
