package iusvyryd.yaus.web.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class RedirectControllerTest {

    @Mock
    private HttpServletRequest req;
    @Mock
    private HttpServletResponse resp;
    @Mock
    private ServletConfig config;
    @Mock
    private ServletContext context;

    private static final String prefix = "http://example.com";

    @Before
    public void setUp() {
        doReturn(context).when(config).getServletContext();
        doReturn(prefix).when(context).getAttribute("prefix");
        when(resp.encodeRedirectURL(anyString())).thenAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocationOnMock) throws Throwable {
                String str = (String)invocationOnMock.getArguments()[0];
                return str;
            }
        });
    }

    @Test
    public void testRedirection() throws ServletException, IOException {
        String redirectTemplate = "{{prefix}}/example/#/error";
        String expectedRedirect = redirectTemplate.replace("{{prefix}}", prefix);
        doReturn(redirectTemplate).when(config).getInitParameter("redirect-url");

        RedirectController c = new RedirectController();
        c.init(config);

        verify(config, times(1)).getInitParameter("redirect-url");
        verify(config, times(1)).getServletContext();

        c.doGet(req, resp);

        verify(resp).sendRedirect(expectedRedirect);
    }

    @Test
    public void testRedirectionWithShort() throws ServletException, IOException {
        String redirectTemplate = "{{prefix}}/example/#/{{short}}";
        String shortId = "qwerty";
        String expectedRedirect = redirectTemplate.replace("{{prefix}}", prefix).replace("{{short}}", shortId);

        doReturn(redirectTemplate).when(config).getInitParameter("redirect-url");
        doReturn("/"+shortId).when(req).getRequestURI();

        RedirectController c = new RedirectController();
        c.init(config);

        verify(config, times(1)).getInitParameter("redirect-url");
        verify(config, times(1)).getServletContext();

        c.doGet(req, resp);

        verify(resp).sendRedirect(expectedRedirect);
    }

}
