package iusvyryd.yaus.web.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import iusvyryd.yaus.model.Url;
import iusvyryd.yaus.service.Service;
import iusvyryd.yaus.exception.YausException;
import iusvyryd.yaus.service.ServiceFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Executors.class, ServiceFactory.class})
public class ResolveControllerTest {

    private HttpServletRequest req;
    private HttpServletResponse resp;
    private ExecutorService executorService;
    private Service service;
    private ServletConfig config;
    private ServletContext context;

    private static final String serviceTemplate = "{{prefix}}/service";
    private static final String prefix = "http://example.com";

    @Before
    public void setUp() {
        req = mock(HttpServletRequest.class);
        resp = mock(HttpServletResponse.class);
        executorService = mock(ExecutorService.class);
        service = mock(Service.class);
        config = mock(ServletConfig.class);
        context = mock(ServletContext.class);

        mockStatic(Executors.class);
        org.powermock.api.mockito.PowerMockito.when(Executors.newFixedThreadPool(anyInt()))
                .thenReturn(executorService);

        mockStatic(ServiceFactory.class);
        org.powermock.api.mockito.PowerMockito.when(ServiceFactory.createService())
                .thenReturn(service);

        doReturn(context).when(config).getServletContext();
        doReturn("1").when(config).getInitParameter("thread-pool-size");
        doReturn(serviceTemplate).when(context).getInitParameter("service-main");
        doReturn(prefix).when(context).getAttribute("prefix");

        when(resp.encodeRedirectURL(anyString())).thenAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocationOnMock) throws Throwable {
                String str = (String)invocationOnMock.getArguments()[0];
                return str;
            }
        });
    }

    @Test
    public void testResolve() throws YausException, ServletException, IOException {
        String shortId = "qwerty";
        String longUrl = "http://resolved.longurl.com";
        Url resolved = new Url(longUrl, shortId);

        doReturn("/" + shortId).when(req).getRequestURI();
        doReturn(resolved).when(service).resolveLongUrl(anyString());

        ResolveController c = new ResolveController();
        c.init(config);

        c.doGet(req, resp);

        verify(resp).sendRedirect(longUrl);
    }

    @Test
    public void testNotResolved() throws YausException, ServletException, IOException {
        String shortId = "qwerty";
        String expected = String.format("%s/#/404/%s",serviceTemplate.replace("{{prefix}}", prefix),
                prefix);
        Url resolved = null;

        doReturn(new StringBuffer(prefix)).when(req).getRequestURL();
        doReturn("/" + shortId).when(req).getRequestURI();
        doReturn(resolved).when(service).resolveLongUrl(anyString());

        ResolveController c = new ResolveController();
        c.init(config);

        c.doGet(req, resp);

        verify(resp).setStatus(HttpServletResponse.SC_NOT_FOUND);
        verify(resp).sendRedirect(expected);
    }

    @Test
    public void testLongShortNotResolved() throws YausException, ServletException, IOException {
        String shortId = "qwerty1111111";
        String expected = String.format("%s/#/404/%s",serviceTemplate.replace("{{prefix}}", prefix),
                prefix);

        doReturn(new StringBuffer(prefix)).when(req).getRequestURL();
        doReturn("/" + shortId).when(req).getRequestURI();

        ResolveController c = new ResolveController();
        c.init(config);

        c.doGet(req, resp);

        verify(resp).setStatus(HttpServletResponse.SC_NOT_FOUND);
        verify(resp).sendRedirect(expected);
    }



}
