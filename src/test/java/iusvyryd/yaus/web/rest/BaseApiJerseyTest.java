package iusvyryd.yaus.web.rest;

import iusvyryd.yaus.exception.YausExceptionMapper;
import org.glassfish.hk2.api.Factory;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.jackson1.Jackson1Feature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.glassfish.jersey.test.inmemory.InMemoryTestContainerFactory;
import org.glassfish.jersey.test.spi.TestContainerException;
import org.glassfish.jersey.test.spi.TestContainerFactory;

import javax.servlet.ServletContext;
import javax.ws.rs.core.Application;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

public abstract class BaseApiJerseyTest extends JerseyTest {

    private static class MockServletContext implements Factory<ServletContext> {

        @Override
        public ServletContext provide() {
            ServletContext sc = mock(ServletContext.class);
            doReturn(prefix).when(sc).getAttribute("prefix");
            doReturn(shortTmpl).when(sc).getInitParameter("service-short");
            doReturn(statsTmpl).when(sc).getInitParameter("service-stats");
            return sc;
        }

        @Override
        public void dispose(ServletContext servletContext) {
        }
    }

    @Override
    protected Application configure() {
        AbstractBinder binder = new AbstractBinder() {
            @Override
            protected void configure() {
                bindFactory(MockServletContext.class).to(ServletContext.class);
            }
        };

        return new ResourceConfig()
                .register(Api.class)
                .register(YausExceptionMapper.class)
                .register(Jackson1Feature.class)
                .register(binder)
                .register(getAdditionalBinder());
    }

    @Override
    protected void configureClient(ClientConfig config) {
        config.register(YausExceptionMapper.class);
        config.register(Jackson1Feature.class);
    }

    @Override
    protected TestContainerFactory getTestContainerFactory() throws TestContainerException {
        return new InMemoryTestContainerFactory();
    }

    protected static final String prefix = "http://example.com";
    protected static final String shortTmpl = "{{prefix}}/short";
    protected static final String statsTmpl = "{{prefix}}/stats";

    protected abstract AbstractBinder getAdditionalBinder();

}
