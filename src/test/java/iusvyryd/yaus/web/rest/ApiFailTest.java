package iusvyryd.yaus.web.rest;

import iusvyryd.yaus.exception.ErrorResponse;
import iusvyryd.yaus.exception.YausException;
import iusvyryd.yaus.model.Url;
import iusvyryd.yaus.service.Service;
import org.codehaus.jackson.map.ObjectMapper;
import org.glassfish.hk2.api.Factory;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

public class ApiFailTest extends BaseApiJerseyTest {

    @Override
    protected AbstractBinder getAdditionalBinder() {
        return new AbstractBinder() {
            @Override
            protected void configure() {
                bindFactory(MockServiceFactory.class).to(Service.class);
            }
        };
    }

    private static class MockServiceFactory implements Factory<Service> {
        @Override
        public Service provide() {
            Service s = mock(Service.class);
            try {
                doThrow(shortEx).when(s).processNewLongUrl(any(Url.class));
                doThrow(statsEx).when(s).resolveLongUrl(anyString());
                doThrow(allEx).when(s).getAllUrls();
            } catch (YausException e) {
                e.printStackTrace();
            }
            return s;
        }

        @Override
        public void dispose(Service service) {
        }
    }

    private static final YausException shortEx =
            new YausException("ex message", Response.Status.BAD_REQUEST);
    private static final YausException statsEx =
            new YausException("ex message", Response.Status.INTERNAL_SERVER_ERROR);
    private static final YausException allEx =
            new YausException("ex message", Response.Status.INTERNAL_SERVER_ERROR);

    @Test
    public void testShort() throws IOException {
        String expected = new ObjectMapper().writeValueAsString(new ErrorResponse(shortEx.getMessage()));

        Response response = target("doshort").request()
                .post(Entity.entity(new Url(), MediaType.APPLICATION_JSON_TYPE));

        assertEquals(shortEx.getStatus().getStatusCode(), response.getStatus());
        assertEquals(expected, response.readEntity(String.class));
    }

    @Test
    public void testStats() throws IOException {
        String expected = new ObjectMapper().writeValueAsString(new ErrorResponse(statsEx.getMessage()));

        Response response = target("getstats/shortid").request().get(Response.class);
        assertEquals(statsEx.getStatus().getStatusCode(), response.getStatus());
        assertEquals(expected, response.readEntity(String.class));
    }

    @Test
    public void testAll() throws IOException {
        String expected = new ObjectMapper().writeValueAsString(new ErrorResponse(allEx.getMessage()));

        Response response = target("all").request().get(Response.class);
        assertEquals(allEx.getStatus().getStatusCode(), response.getStatus());
        assertEquals(expected, response.readEntity(String.class));
    }
}
