package iusvyryd.yaus.web.rest;

import iusvyryd.yaus.exception.YausException;
import iusvyryd.yaus.model.Country;
import iusvyryd.yaus.model.Statistics;
import iusvyryd.yaus.model.Url;
import iusvyryd.yaus.service.Service;
import org.codehaus.jackson.map.ObjectMapper;
import org.glassfish.hk2.api.Factory;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

public class ApiSuccessTest extends BaseApiJerseyTest {

    @Override
    protected AbstractBinder getAdditionalBinder() {
        return new AbstractBinder() {
            @Override
            protected void configure() {
                bindFactory(MockServiceFactory.class).to(Service.class);
            }
        };
    }

    private static class MockServiceFactory implements Factory<Service> {
        @Override
        public Service provide() {
            Service s = mock(Service.class);
            try {
                doReturn(shortResult).when(s).processNewLongUrl(any(Url.class));
                doReturn(shortResult).when(s).resolveLongUrl(anyString());
                doReturn(statsResult).when(s).getFullStatistics(any(Url.class));
                doReturn(allResult).when(s).getAllUrls();
            } catch (YausException e) {
                e.printStackTrace();
            }
            return s;
        }

        @Override
        public void dispose(Service service) {
        }
    }

    private static final Url shortResult;
    private static final Statistics statsResult;
    private static final List<Url> allResult;

    static {
        shortResult = new Url("http://long.url", "shortid");
        shortResult.setShortUrl(shortTmpl.replace("{{prefix}}", prefix));
        shortResult.setStatsUrl(statsTmpl.replace("{{prefix}}", prefix));
        statsResult = new Statistics(shortResult);
        Map<Country, Long> cm = new LinkedHashMap<>();
        cm.put(new Country("countryname", "cn"), 10L);
        statsResult.setGroupedByCountry(cm);
        Map<Date, Long> dm = new LinkedHashMap<>();
        dm.put(new Date(), 5L);
        statsResult.setGroupedByDate(dm);
        allResult = new ArrayList<>();
        allResult.add(shortResult);
    }

    @Test
    public void testShort() throws IOException {
        String expected = new ObjectMapper().writeValueAsString(shortResult);

        Response response = target("doshort").request()
                .post(Entity.entity(shortResult, MediaType.APPLICATION_JSON_TYPE));

        assertEquals(200, response.getStatus());
        assertEquals(expected, response.readEntity(String.class));
    }

    @Test
    public void testStats() throws IOException {
        String expected = new ObjectMapper().writeValueAsString(statsResult);

        Response response = target("getstats/shortid").request().get(Response.class);
        assertEquals(200, response.getStatus());
        assertEquals(expected, response.readEntity(String.class));
    }

    @Test
    public void testAll() throws IOException {
        String expected = new ObjectMapper().writeValueAsString(allResult);

        Response response = target("all").request().get(Response.class);
        assertEquals(200, response.getStatus());
        assertEquals(expected, response.readEntity(String.class));
    }

}
