package iusvyryd.yaus.web.filter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class EncodingFilterTest {

    @Mock
    private HttpServletRequest req;
    @Mock
    private HttpServletResponse resp;
    @Mock
    private FilterChain chain;
    @Mock
    private FilterConfig config;

    private final String enc = "UTF-8";

    @Before
    public void setup() {
        doReturn(enc).when(config).getInitParameter("encoding");
        doReturn("/static/::/webjars/").when(config).getInitParameter("exclude");
    }

    @Test
    public void testSetupEncoding() throws ServletException, IOException {
        doReturn("/some/path/").when(req).getRequestURI();

        EncodingFilter f = new EncodingFilter();
        f.init(config);
        f.doHttpFilter(req, resp, chain);

        verify(req).setCharacterEncoding(enc);
        verify(resp).setCharacterEncoding(enc);
    }

    @Test
    public void testExcluded() throws ServletException, IOException {
        doReturn("/some/path/webjars/more.path").when(req).getRequestURI();

        EncodingFilter f = new EncodingFilter();
        f.init(config);
        f.doHttpFilter(req, resp, chain);

        verify(req, never()).setCharacterEncoding(anyString());
        verify(resp, never()).setCharacterEncoding(anyString());
    }

}
