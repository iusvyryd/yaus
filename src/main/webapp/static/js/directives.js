"use strict";

var directives = angular.module("directives", []);

directives.directive("loading", ["$http",
    function ($http) {
        return {
            restrict: "E",
            template: "<div class='loading'><div class='loading-bar'></div><div class='loading-bar'></div><div class='loading-bar'></div><div class='loading-bar'></div></div>",
            link: function (scope, element, attrs) {
                scope.isLoading = function () {
                    return $http.pendingRequests.length > 0;
                };

                scope.$watch(scope.isLoading, function (load) {
                    if (load) {
                        element.css("display", "block");
                    }
                    else {
                        element.css("display", "none");
                    }
                });
            }
        };
    }
]);