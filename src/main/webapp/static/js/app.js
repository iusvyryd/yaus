"use strict";

var app = angular.module("app", [
    "ngRoute",
    "controllers",
    "directives"
]);

app.config(["$routeProvider",
    function($routeProvider) {
        $routeProvider.
            when("/404/:notFoundUrl*", {
                templateUrl: "static/view/404.html"
            }).
            when("/404/", {
                templateUrl: "static/view/404.html"
            }).
            when("/stats/:shortId", {
                templateUrl: "static/view/stats.html"
            }).
            when("/list/", {
                templateUrl: "static/view/list.html"
            }).
            otherwise({
                templateUrl: "static/view/main.html"
            });
    }
]);
