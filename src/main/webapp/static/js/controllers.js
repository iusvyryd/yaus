"use strict";

var controllers = angular.module("controllers", ["highcharts-ng"]);

function responseHandler(scopeObj, data, success) {
    console.log("response:"+success, data);
    scopeObj.data = data;
    scopeObj.received = true;
    scopeObj.success = success;
}

controllers.controller("MainController", ["$scope", "$http",
    function($scope, $http) {

        $scope.main = {
            newLongUrl: "",
            data: {},
            received: false,
            success: false,
            makeShort: function() {
                $scope.received = false;
                var postData = {
                    longUrl: $scope.main.newLongUrl
                };
                var promise = $http.post("api/doshort", postData, {});
                promise.success(function(data, status) {
                    responseHandler($scope.main, data, true);
                });
                promise.error(function(data, status) {
                    responseHandler($scope.main, data, false);
                });
            }
        };
    }
]);

controllers.controller("StatsController", ["$scope", "$http", "$routeParams",
    function($scope, $http, $routeParams) {

        function configureChart() {
            $scope.stats.chartConfig.title.text += $scope.stats.data.totalCount;

            var seriesData = [];
            for (var key in $scope.stats.data.groupedByDate) {
                $scope.stats.chartConfig.xAxis.categories.push(key);
                seriesData.push($scope.stats.data.groupedByDate[key]);
            }
            $scope.stats.chartConfig.series.push({
                name: $scope.stats.data.url.shortUrl,
                data: seriesData
            });
        }

        $scope.stats = {
            data: {},
            received: false,
            success: false,
            chartConfig: {
                options: {
                    chart: {
                        margin: [50, 50, 50, 50],
                        type: 'line'
                    }
                },
                title: {
                    text: 'Total clicks: '
                },
                xAxis: {
                    categories: []
                },
                yAxis: {
                    title: {
                        text: ' '
                    }
                },
                series: []
            },
            getStats: function() {
                var promise = $http.get("api/getstats/" + $routeParams.shortId, {});
                promise.success(function(data) {
                    responseHandler($scope.stats, data, true);
                    configureChart();
                });
                promise.error(function(data, status) {
                    responseHandler($scope.stats, data, false);
                    configureChart();
                });
            }
        };

        $scope.stats.getStats();
    }
]);

controllers.controller("AllController", ["$scope", "$http", "$routeParams",
    function($scope, $http, $routeParams) {

        $scope.all = {
            data: {},
            received: false,
            success: false,
            getAll: function() {
                var promise = $http.get("api/all", {});
                promise.success(function(data) {
                    responseHandler($scope.all, data, true);
                });
                promise.error(function(data, status) {
                    responseHandler($scope.all, data, false);
                });
            }
        };

        $scope.all.getAll();
    }
]);

controllers.controller("NotFoundController", ["$scope", "$routeParams",
    function($scope, $routeParams) {
        $scope.notFound = {
            notFoundUrl: $routeParams.notFoundUrl
        };
    }
]);
