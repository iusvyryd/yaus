CREATE DATABASE IF NOT EXISTS yaus;
GRANT ALL PRIVILEGES ON yaus.* TO 'yaususer'@'%'  WITH GRANT OPTION;

USE yaus;

CREATE TABLE IF NOT EXISTS urls (
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    long_url TEXT NOT NULL,
    long_url_hash INTEGER NOT NULL,
    short_id VARCHAR(20) NOT NULL,
    creation_timestamp DATETIME
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE urls
	ADD UNIQUE INDEX long_url_hash_index (long_url_hash);

ALTER TABLE urls
	ADD UNIQUE INDEX short_id_path_index (short_id);

CREATE TABLE IF NOT EXISTS url_clicks (
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    browser VARCHAR(50),
    referer VARCHAR(50),
    platform VARCHAR(30),
    country_name VARCHAR(80),
    country_iso VARCHAR(6),
    creation_date DATE NOT NULL,
    url_id INTEGER NOT NULL,
    FOREIGN KEY (url_id) REFERENCES urls(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
