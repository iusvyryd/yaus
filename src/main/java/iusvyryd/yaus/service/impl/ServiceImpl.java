package iusvyryd.yaus.service.impl;

import org.apache.log4j.Logger;
import iusvyryd.yaus.dao.DaoFactory;
import iusvyryd.yaus.dao.ClickDao;
import iusvyryd.yaus.dao.UrlDao;
import iusvyryd.yaus.model.Country;
import iusvyryd.yaus.model.Click;
import iusvyryd.yaus.model.Statistics;
import iusvyryd.yaus.model.Url;
import iusvyryd.yaus.service.Service;
import iusvyryd.yaus.exception.YausException;
import iusvyryd.yaus.util.CodecFactory;
import iusvyryd.yaus.util.geoip.GeoIP;
import iusvyryd.yaus.util.HashAlgoFactory;
import iusvyryd.yaus.util.ua.UAParser;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.ws.rs.core.Response;
import java.net.URL;
import java.util.List;

/**
 * Concrete implementation of {@link iusvyryd.yaus.service.Service} interface
 */
public class ServiceImpl implements Service {

    private static final Logger logger = Logger.getLogger(ServiceImpl.class);

    public ServiceImpl() {
    }

    private UrlDao createUrlDao() {
        return DaoFactory.createDaoFactory().createUrlDao();
    }

    private ClickDao createClickDao() {
        return DaoFactory.createDaoFactory().createClickDao();
    }

    private void closeDao(AutoCloseable dao) {
        try{
            dao.close();
        } catch (Exception e) {
        }
    }

    @Override
    public Url processNewLongUrl(Url url) throws YausException {
        if (url == null || url.getLongUrl() == null) {
            throw new YausException("long url not specified", Response.Status.BAD_REQUEST);
        }

        // encode first
        url.setLongUrl(CodecFactory.defaultUrlCodec().encode(url.getLongUrl()));
        int hash = HashAlgoFactory.defaultHashAlgo().hash(url.getLongUrl().getBytes());
        url.setLongUrlHash(hash);

        UrlDao dao = createUrlDao();
        try {
            Url existing = dao.findByLongUrlHash(hash);
            return existing;
        } catch (NoResultException e) {
        } catch (NonUniqueResultException e) {
            closeDao(dao);
            throw new YausException("more than 1 same url already exists", Response.Status.INTERNAL_SERVER_ERROR, e);
        } catch (Exception e) {
            closeDao(dao);
            throw new YausException("failed to check url existence", Response.Status.INTERNAL_SERVER_ERROR, e);
        }

        // generate short
        url.setShortId(CodecFactory.defaultHashCodec().encode(hash));
        try {
            logger.debug("saving new url " + url);
            return dao.save(url);
        } catch (Exception e) {
            throw new YausException("failed to save url",
                    Response.Status.INTERNAL_SERVER_ERROR, e);
        } finally {
            closeDao(dao);
        }
    }

    @Override
    public Url resolveLongUrl(String shortId) throws YausException {
        if (shortId == null || shortId.length() >= 10) {
            throw new YausException("unknown format of short url", Response.Status.BAD_REQUEST);
        }

        UrlDao dao = createUrlDao();
        try {
            return dao.findByShortId(shortId);
        } catch (NoResultException e) {
            throw new YausException("short url not found '" + shortId + "'",
                    Response.Status.INTERNAL_SERVER_ERROR, e);
        } catch (NonUniqueResultException e) {
            throw new YausException("more than 1 same short id found '" + shortId + "'",
                    Response.Status.INTERNAL_SERVER_ERROR, e);
        } catch (Exception e) {
            throw new YausException("search long url error '" + shortId + "'",
                    Response.Status.INTERNAL_SERVER_ERROR, e);
        } finally {
            closeDao(dao);
        }
    }

    @Override
    public Click processNewClick(Click click, String ip, String userAgent, String referer)
            throws YausException
    {
        // referer
        if (referer != null) {
            try {
                referer = new URL(referer).getHost();
            }
            catch (Exception e) {
                referer = null;
            }
        }
        click.setReferer(referer);
        try {
            // geoip
            GeoIP.CountryInfo cinfo = new GeoIP().resolveCountryInfo(ip);
            click.setCountry(new Country(cinfo.name(), cinfo.iso()));
        }
        catch (IllegalStateException e) {
            logger.warn("geoip seems not to be opened", e);
        }
        // user agent
        UAParser.UAInfo uainfo = UAParser.parse(userAgent);

        click.setBrowser(uainfo.browser());
        click.setPlatform(uainfo.platform());

        try (ClickDao dao = createClickDao()) {
            logger.debug("saving new click " + click);
            dao.save(click);
        } catch (Exception e) {
            throw new YausException("failed to save click");
        }
        return click;
    }

    @Override
    public Statistics getFullStatistics(Url url) throws YausException {
        if (url == null) {
            throw new YausException("long url not specified", Response.Status.BAD_REQUEST);
        }

        try (ClickDao dao = createClickDao()) {
            Statistics stat = dao.totalStatisticsReport(url);
            return stat;
        }
        catch (Exception e) {
            throw new YausException("failed to collect statistics",
                    Response.Status.INTERNAL_SERVER_ERROR, e);
        }
    }

    @Override
    public List<Url> getAllUrls() throws YausException {
        try (UrlDao dao = createUrlDao()) {
            return dao.getAll();
        }
        catch (Exception e) {
            throw new YausException("failed to get all urls list",
                    Response.Status.INTERNAL_SERVER_ERROR, e);
        }
    }
}
