package iusvyryd.yaus.service;

import iusvyryd.yaus.exception.YausException;
import iusvyryd.yaus.model.Click;
import iusvyryd.yaus.model.Statistics;
import iusvyryd.yaus.model.Url;

import java.util.List;

/**
 * Service interface
 * Defines basic functionality that must implement
 * objects that works between web ant data access on service layer
 */
public interface Service {

    /**
     * Creation of new short url from long
     *
     * @param url url object with long representation
     * @return fully filled url object with long, long hash and short id
     * @throws iusvyryd.yaus.exception.YausException if fatal exceptions in data access tier occurs
     */
    public Url processNewLongUrl(Url url) throws YausException;

    /**
     * Retrieves full url object from short url id
     *
     * @param shortId short id
     * @return url
     * @throws iusvyryd.yaus.exception.YausException if exception in data access tier occurs or if no url was resolved
     */
    public Url resolveLongUrl(String shortId) throws YausException;

    /**
     * Creates <code>Click</code> object from input arguments, and store it
     *
     * @param click <code>Click</code> object
     * @param ip ip address of client
     * @param userAgent user agent string
     * @param referer from what resource client comes
     * @return <code>Click</code> object
     * @throws iusvyryd.yaus.exception.YausException  if exception in data access tier occurs
     */
    public Click processNewClick(Click click, String ip, String userAgent, String referer)
            throws YausException;

    /**
     * Retrieve full grouped statistics for <code>Url</code>
     *
     * @param url <code>Url</code> object
     * @return full <code>Statistics</code> for  <code>Url</code>
     * @throws iusvyryd.yaus.exception.YausException if exception in data access tier occurs
     */
    public Statistics getFullStatistics(Url url) throws YausException;

    /**
     * Retrieve all long urls that was shorted
     *
     * @return list of urls
     * @throws iusvyryd.yaus.exception.YausException if exception in data access tier occurs
     */
    public List<Url> getAllUrls() throws YausException;
}
