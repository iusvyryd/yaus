package iusvyryd.yaus.service;

import iusvyryd.yaus.service.impl.ServiceImpl;
import org.glassfish.hk2.api.Factory;

/**
 * Factory for service creation
 * Actually always returns singleton instance as service is stateless
 *
 * implements {@link org.glassfish.hk2.api.Factory} for jersey injections compatability
 */
public class ServiceFactory implements Factory<Service> {

    private final static Service service = new ServiceImpl();

    public static Service createService() {
        return service;
    }

    @Override
    public Service provide() {
        return service;
    }

    @Override
    public void dispose(Service service) {

    }
}
