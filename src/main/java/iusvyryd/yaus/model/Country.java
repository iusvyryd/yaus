package iusvyryd.yaus.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Simple java bean object that serves as part of click entity and represent country information
 * Also used as key in grouped by country statistics map
 */
@Embeddable
public class Country {

    @Column(name = "country_name", nullable = true, length = 80)
    private String name;

    @Column(name = "country_iso", nullable = true, length = 6)
    private String iso;

    public Country() {
    }

    public Country(String name, String iso) {
        this.name = name;
        this.iso = iso;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Country)) {
            return false;
        }

        Country other = (Country) o;
        if (name != null) {
            if (!name.equals(other.name)) {
                return false;
            }
        }
        else {
            if (other.name != null) {
                return false;
            }
        }
        if (iso != null) {
            if (!iso.equals(other.iso)) {
                return false;
            }
        }
        else {
            if (other.iso != null) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (iso != null ? iso.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Country{" +
                "name='" + name + '\'' +
                ", iso='" + iso + '\'' +
                '}';
    }
}
