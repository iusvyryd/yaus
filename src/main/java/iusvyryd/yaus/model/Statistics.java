package iusvyryd.yaus.model;

import iusvyryd.yaus.util.serializer.CountryMapSerializer;
import iusvyryd.yaus.util.serializer.DateMapKeySerializer;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.Date;
import java.util.Map;

/**
 * Object that represents grouped statistics of
 * <code>Click</code> on <code>Url</code>
 */
public class Statistics {

    private Url url;

    private long totalCount = 0;

    @JsonSerialize(keyUsing = DateMapKeySerializer.class)
    private Map<Date, Long> groupedByDate;

    private Map<String, Long> groupedByBrowser;

    @JsonSerialize(using = CountryMapSerializer.class)
    private Map<Country, Long> groupedByCountry;

    private Map<String, Long> groupedByPlatform;

    private Map<String, Long> groupedByReferer;

    public Statistics() {

    }

    public Statistics(Url url) {
        this.url = url;
    }

    public Url getUrl() {
        return url;
    }

    public void setUrl(Url url) {
        this.url = url;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public Map<Date, Long> getGroupedByDate() {
        return groupedByDate;
    }

    public void setGroupedByDate(Map<Date, Long> groupedByDate) {
        this.groupedByDate = groupedByDate;
    }

    public Map<String, Long> getGroupedByBrowser() {
        return groupedByBrowser;
    }

    public void setGroupedByBrowser(Map<String, Long> groupedByBrowser) {
        this.groupedByBrowser = groupedByBrowser;
    }

    public Map<Country, Long> getGroupedByCountry() {
        return groupedByCountry;
    }

    public void setGroupedByCountry(Map<Country, Long> groupedByCountry) {
        this.groupedByCountry = groupedByCountry;
    }

    public Map<String, Long> getGroupedByPlatform() {
        return groupedByPlatform;
    }

    public void setGroupedByPlatform(Map<String, Long> groupedByPlatform) {
        this.groupedByPlatform = groupedByPlatform;
    }

    public Map<String, Long> getGroupedByReferer() {
        return groupedByReferer;
    }

    public void setGroupedByReferer(Map<String, Long> groupedByReferer) {
        this.groupedByReferer = groupedByReferer;
    }

}
