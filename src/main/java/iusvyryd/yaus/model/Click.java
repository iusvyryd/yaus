package iusvyryd.yaus.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Java bean domain object representing one click on short url
 */
@Entity
@Table(name = "url_clicks")
@Access(AccessType.FIELD)
public class Click extends BaseEntity {

    @Column(name = "browser", nullable = true, length = 50)
    private String browser;

    @Embedded
    private Country country;

    @Column(name = "referer", nullable = true, length = 50)
    private String referer;

    @Column(name = "platform", nullable = true, length = 30)
    private String platform;

    @Temporal(TemporalType.DATE)
    @Column(name = "creation_date", updatable = false, nullable = false)
    private Date creationDate;

    @ManyToOne
    @JoinColumn(name = "url_id", nullable = false)
    private Url url;

    @PrePersist
    void createCreationDate() {
        this.creationDate = new Date();
    }

    public Click() {
    }

    public Click(Url url) {
        this.url = url;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public String getReferer() {
        return referer;
    }

    public void setReferer(String referer) {
        this.referer = referer;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public Url getUrl() {
        return url;
    }

    public void setUrl(Url url) {
        this.url = url;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    @Override
    public String toString() {
        return "Click{" +
                "browser='" + browser + '\'' +
                ", country=" + country +
                ", referer='" + referer + '\'' +
                ", platform='" + platform + '\'' +
                ", url=" + url +
                '}';
    }
}
