package iusvyryd.yaus.model;

import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

/**
 * Java bean domain object that binds long url to its short representation
 */
@Entity
@Table(name = "urls",
    indexes = {
            @Index(name = "long_url_hash_index", columnList = "long_url_hash", unique = true),
            @Index(name = "short_id_path_index", columnList = "short_id", unique = true)
    })
@Access(AccessType.FIELD)
public class Url extends BaseEntity {

    @Column(name = "long_url", nullable = false, columnDefinition = "TEXT")
    private String longUrl;

    @JsonIgnore
    @Column(name = "long_url_hash", nullable = false)
    private Integer longUrlHash;

    @JsonIgnore
    @Column(name = "short_id", nullable = false, length = 20)
    private String shortId;

    @JsonIgnore
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "creation_timestamp", updatable = false)
    private Date creationTimestamp;

    @Transient
    private String shortUrl;

    @Transient
    private String statsUrl;

    @PrePersist
    void createCreationTimestamp() {
        this.creationTimestamp = new Date();
    }

    public Url() {
    }

    public Url(String longUrl) {
        this.longUrl = longUrl;
    }

    public Url(String longUrl, String shortId) {
        this.longUrl = longUrl;
        this.shortId = shortId;
    }

    public String getLongUrl() {
        return longUrl;
    }

    public void setLongUrl(String longUrl) {
        this.longUrl = longUrl;
    }

    public Integer getLongUrlHash() {
        return longUrlHash;
    }

    public void setLongUrlHash(Integer longUrlHash) {
        this.longUrlHash = longUrlHash;
    }

    public String getShortId() {
        return shortId;
    }

    public void setShortId(String shortId) {
        this.shortId = shortId;
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    public String getStatsUrl() {
        return statsUrl;
    }

    public void setStatsUrl(String statsUrl) {
        this.statsUrl = statsUrl;
    }

    @Override
    public String toString() {
        return "Url{" +
                "longUrl='" + longUrl + '\'' +
                ", longUrlHash=" + longUrlHash +
                ", shortId='" + shortId + '\'' +
                '}';
    }
}
