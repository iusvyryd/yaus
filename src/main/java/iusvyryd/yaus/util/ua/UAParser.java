package iusvyryd.yaus.util.ua;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility class
 * Helps to parse user agent and retrieve user browser family and user platform family
 */
public class UAParser {

    public static class UAInfo {
        private final String browser;
        private final String platform;
        private UAInfo(String browser, String platform) {
            this.browser = browser;
            this.platform = platform;
        }
        public String browser() {
            return browser;
        }
        public String platform() {
            return platform;
        }
    }

    private static Pattern ptrOsInfo;
    private static Pattern ptrOs;
    private static Pattern ptrBrowser;
    private static Pattern ptrFinal;

    static {
        ptrOsInfo = Pattern.compile("\\((.*?)\\)", Pattern.CASE_INSENSITIVE);
        ptrOs = Pattern.compile("(BB\\d+;|Android|CrOS|iPhone|iPad|Linux|" +
                "Macintosh|Windows(\\ Phone)?|Silk|linux-gnu|" +
                "BlackBerry|PlayBook|(New\\ )?Nintendo\\ (WiiU?|3DS)|Xbox(\\ One)?)", Pattern.CASE_INSENSITIVE);
        ptrBrowser = Pattern.compile("(Camino|Kindle(\\ Fire\\ Build)?|Firefox|"+
                "Iceweasel|Safari|MSIE|Trident|AppleWebKit|Chrome|" +
                "Vivaldi|IEMobile|Opera|OPR|Silk|Midori|Edge|" +
                "Baiduspider|Googlebot|YandexBot|bingbot|Lynx|Wget|curl|" +
                "NintendoBrowser|PLAYSTATION\\ (\\d|Vita)+)", Pattern.CASE_INSENSITIVE);
        ptrFinal = Pattern.compile("^(?!Mozilla)([A-Z0-9\\-]+)(/([0-9A-Z.]+))?([;| ]\\ ?.*)?$", Pattern.CASE_INSENSITIVE);
    }

    private UAParser() {
    }

    public static UAInfo parse(String userAgent) {
        String platform = null;
        String browser = null;

        try {
            // platform
            Matcher mOsInfo = ptrOsInfo.matcher(userAgent);
            if (mOsInfo.find()) {
                String osInfo = mOsInfo.group(1);

                Matcher mOs= ptrOs.matcher(osInfo);
                Set<String> platforms = new LinkedHashSet<>();
                while (mOs.find()) {
                    platforms.add(mOs.group(1));
                }
                Set<String> priority = new LinkedHashSet<>(Arrays.asList("Android", "Xbox One", "Xbox"));
                if (platforms.size() > 1) {
                    priority.retainAll(platforms);
                    if (priority.size() > 0) {
                        platform = priority.iterator().next();
                    }
                    else {
                        platform = platforms.iterator().next();
                    }
                }
                else {
                    platform = platforms.iterator().next();
                }
                if (platform.equals("linux-gnu")) {
                    platform = "Linux";
                }
                else if(platform.equals("CrOS")) {
                    platform = "Chrome OS";
                }
            }

            // browser
            Matcher mBrowser = ptrBrowser.matcher(userAgent);
            Set<String> browsers = new LinkedHashSet<>();
            while (mBrowser.find()) {
                browsers.add(mBrowser.group(1));
            }
            if (browsers.size() == 0) {
                if (platform == null) {
                    Matcher mFinal = ptrFinal.matcher(userAgent);
                    if (mFinal.find()) {
                        browser = mFinal.group(1);
                        return new UAInfo(browser, platform);
                    }
                }
                return new UAInfo(null, null);
            }

            browser = browsers.iterator().next();
            if (browsers.contains("Iceweasel")) {
                browser = "Firefox";
            }
            else if (browsers.contains("Playstation Vita")) {
                platform = "PlayStation Vita";
                browser  = "Browser";
            }
            else if (browsers.contains("Kindle Fire Build")) {
                browser  = "Kindle";
                platform = "Kindle Fire";
            }
            else if (browsers.contains("Silk")) {
                browser  = "Silk";
                platform = "Kindle Fire";
            }
            else if (browsers.contains("NintendoBrowser") || browsers.contains("Nintendo 3DS")) {
                browser = "NintendoBrowser";
            }
            else if (browsers.contains("Kindle")) {
                browser  = "Kindle";
                platform = "Kindle";
            }
            else if (browsers.contains("OPR")) {
                browser  = "Opera Next";
            }
            else if (browsers.contains("Opera")) {
                browser  = "Opera";
            }
            else if (browsers.contains("Midori")) {
                browser  = "Midori";
            }
            else if ("MSIE".equals(browser) || browsers.contains("Trident") || browsers.contains("Edge")) {
                browser  = "MSIE";
                if (browsers.contains("IEModile")) {
                    browser = "IEMobile";
                }
            }
            else if (browsers.contains("Vivaldi")) {
                browser = "Vivaldi";
            }
            else if (browsers.contains("Chrome")) {
                browser = "Chrome";
            }
            else if ("AppleWebKit".equals(browser)) {
                if ("Android".equals(platform)) {
                    browser = "Android Browser";
                }
                else if (platform.indexOf("BB") == 0) {
                    browser  = "BlackBerry Browser";
                    platform = "BlackBerry";
                }
                else if ("BlackBerry".equals(platform) || "PlayBook".equals(platform)) {
                    browser = "BlackBerry Browser";
                }
                else if (browsers.contains("Safari")) {
                    browser = "Safari";
                }
            }
        }
        catch (Exception e) {}

        return new UAInfo(browser, platform);
    }

}
