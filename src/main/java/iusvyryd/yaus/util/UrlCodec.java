package iusvyryd.yaus.util;

/**
 * Common interface for Url codec
 */
public interface UrlCodec extends Codec<String, String> {
}
