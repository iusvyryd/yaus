package iusvyryd.yaus.util;

/**
 * Codec interface for hash codec that encodes hash to string
 */
public interface HashCodec extends Codec<Integer, String> {
}
