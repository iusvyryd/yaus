package iusvyryd.yaus.util.codec;

import org.apache.log4j.Logger;
import iusvyryd.yaus.util.HashCodec;

public class Base62Codec implements HashCodec {

    private static final Logger logger = Logger.getLogger(Base62Codec.class);

    private static final String ALPHABET =
            "aZbYc0XdWeV1fUgTh2SiRjQ3kPlOm4NnMoL5pKqJr6IsHtG7uFvEw8DxCyB9zA";

    private static final int BASE = ALPHABET.length();
    private static final char NEGATIVE_IDENTIFIER = '_';

    @Override
    public String encode(Integer data) {
        int i = data.intValue();
        if (i == 0) {
            return Character.toString(ALPHABET.charAt(0));
        }

        boolean isNegative = i < 0;
        if (isNegative) {
            i = -i;
        }

        StringBuilder sb = new StringBuilder();
        int reminder;
        while (i > 0) {
            reminder = i % BASE;
            sb.append(ALPHABET.charAt(reminder));
            i /= BASE;
        }
        if (isNegative) {
            long pos = System.currentTimeMillis() % sb.length();
            sb.insert((int)pos, NEGATIVE_IDENTIFIER);
        }

        String result = sb.reverse().toString();
        logger.debug("encoding result " + data + " ==> " + result);
        return result;
    }

    @Override
    public Integer decode(String data) {
        int i = 0;
        char[] chars = data.toCharArray();
        for (char c : chars) {
            if (c != NEGATIVE_IDENTIFIER) {
                i = i * BASE + ALPHABET.indexOf(c);
            }
        }

        Integer result = data.indexOf(NEGATIVE_IDENTIFIER) == -1 ? i : -i;
        logger.debug("encoding result " + data + " ==> " + result);
        return result;
    }
}
