package iusvyryd.yaus.util.codec;

import iusvyryd.yaus.util.UrlCodec;

import java.net.IDN;
import java.net.MalformedURLException;
import java.net.URL;

public class PunyCodeCodec implements UrlCodec {

    @Override
    public String encode(String data) {
        try {
            URL url = new URL(data);
            StringBuilder sb = new StringBuilder();
            sb.append(url.getProtocol()).append("://");
            if (url.getUserInfo() != null) {
                sb.append(url.getUserInfo()).append("@");
            }
            sb.append(IDN.toASCII(url.getHost())).append(url.getFile());
            if (url.getRef() != null) {
                sb.append("#").append(url.getRef());
            }
            return sb.toString();
        } catch (MalformedURLException e) {
        }
        return null;
    }

    @Override
    public String decode(String data) {
        try {
            URL url = new URL(data);
            StringBuilder sb = new StringBuilder();
            sb.append(url.getProtocol()).append("://");
            if (url.getUserInfo() != null) {
                sb.append(url.getUserInfo()).append("@");
            }
            sb.append(IDN.toUnicode(url.getHost())).append(url.getFile());
            if (url.getRef() != null) {
                sb.append("#").append(url.getRef());
            }
            return sb.toString();
        } catch (MalformedURLException e) {
        }
        return null;
    }

}
