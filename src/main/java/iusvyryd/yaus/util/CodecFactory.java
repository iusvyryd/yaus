package iusvyryd.yaus.util;

import iusvyryd.yaus.util.codec.Base62Codec;
//import iusvyrd.yaus.util.codec.GsonJsonCodec;
import iusvyryd.yaus.util.codec.PunyCodeCodec;

/**
 * Factory for creation default codec
 */
public class CodecFactory {

    private CodecFactory() {
    }

    /**
     * Create default {@link iusvyryd.yaus.util.UrlCodec}
     * @return
     */
    public static UrlCodec defaultUrlCodec() {
        return new PunyCodeCodec();
    }

    /**
     * Create default {@link iusvyryd.yaus.util.HashCodec}
     * @return
     */
    public static HashCodec defaultHashCodec() {
        return new Base62Codec();
    }

    /**
     * Create default {@link iusvyryd.yaus.util.JsonCodec}
     * @param type
     * @param <T>
     * @return
     */
    /*@Deprecated
    public static <T> JsonCodec<T> createJsonCodec(Class<T> type) {
        return new GsonJsonCodec<T>(type);
    }*/

}
