package iusvyryd.yaus.util.hash;

import iusvyryd.yaus.util.HashAlgo;

public final class MurMur2 implements HashAlgo {

    private static final int seed = 0x7e709ed8;

    private int hash(byte[] data, int pos, int len, int seed) {
        final int m = 0x5bd1e995;
        final int r = 24;

        int h = seed ^ len;
        int len4 = len >>> 2;

        for (int i = pos ; i < len4; i++) {
            final int i4 = i << 2;
            int k = data[i4 + 3] & 0xff;
            k = k << 8;
            k |= (data[i4 + 2] & 0xff);
            k = k << 8;
            k |= (data[i4 + 1] & 0xff);
            k = k << 8;
            k |= (data[i4] & 0xff);
            k *= m;
            k ^= k >>> r;
            k *= m;
            h *= m;
            h ^= k;
        }

        int lenm = len4 << 2;
        int left = len - lenm;
        if (left != 0) {
            if (left >= 3) {
                h ^= data[len - 3] << 16;
            }
            else if (left >= 2) {
                h ^= data[len - 2] << 8;
            }
            else if (left >= 1) {
                h ^= data[len - 1];
            }
            h *= m;
        }
        h ^= h >>> 13;
        h *= m;
        h ^= h >>> 15;

        return h;
    }

    @Override
    public int hash(byte[] data) {
        return hash(data, 0, data.length, seed);
    }

    @Override
    public int hash(byte[] data, int pos, int len) {
        return hash(data, pos, len, seed);
    }

}
