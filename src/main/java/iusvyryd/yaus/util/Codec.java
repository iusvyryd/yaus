package iusvyryd.yaus.util;

/**
 * Common Codec interface
 * Defines methods for encoding <T> to <E> and decoding <E> to <T>
 *
 * @param <T>
 * @param <E>
 */
public interface Codec<T, E> {

    public E encode(T data);
    public T decode(E data);

}
