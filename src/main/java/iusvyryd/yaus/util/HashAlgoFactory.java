package iusvyryd.yaus.util;

import iusvyryd.yaus.util.hash.MurMur2;

/**
 * Factory for hash algorithms creation
 */
public class HashAlgoFactory {

    private HashAlgoFactory() {
    }

    public static HashAlgo defaultHashAlgo() {
        return new MurMur2();
    }

}
