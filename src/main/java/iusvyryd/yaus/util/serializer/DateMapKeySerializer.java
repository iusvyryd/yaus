package iusvyryd.yaus.util.serializer;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateMapKeySerializer extends JsonSerializer<Date> {

    private static final DateFormat df = new SimpleDateFormat("yyyy/MM/dd");

    @Override
    public void serialize(Date date, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
            throws IOException, JsonProcessingException
    {
        jsonGenerator.writeFieldName(df.format(date));
    }

}
