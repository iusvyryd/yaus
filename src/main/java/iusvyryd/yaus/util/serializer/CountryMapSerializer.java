package iusvyryd.yaus.util.serializer;

import iusvyryd.yaus.model.Country;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;

import java.io.IOException;
import java.util.Map;

public class CountryMapSerializer extends JsonSerializer<Map<Country, Long>> {
    @Override
    public void serialize(Map<Country, Long> countryLongMap, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
            throws IOException, JsonProcessingException
    {
        jsonGenerator.writeStartArray();
        for (Map.Entry<Country, Long> kv : countryLongMap.entrySet()) {
            jsonGenerator.writeStartArray();
            jsonGenerator.writeStartObject();
            jsonGenerator.writeFieldName("name");
            jsonGenerator.writeString(kv.getKey().getName());
            jsonGenerator.writeFieldName("iso");
            jsonGenerator.writeString(kv.getKey().getIso());
            jsonGenerator.writeEndObject();
            jsonGenerator.writeNumber(kv.getValue());
            jsonGenerator.writeEndArray();
        }
        jsonGenerator.writeEndArray();
    }

}
