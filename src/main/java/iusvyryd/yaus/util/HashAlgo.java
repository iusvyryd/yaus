package iusvyryd.yaus.util;

/**
 * Common interface for Hash algorithms
 */
public interface HashAlgo {

    public int hash(final byte[] data);
    public int hash(final byte[] data, int pos, int len);

}
