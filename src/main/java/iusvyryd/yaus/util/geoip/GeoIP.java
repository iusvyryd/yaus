package iusvyryd.yaus.util.geoip;

import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.model.CountryResponse;
import com.maxmind.geoip2.record.Country;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

/**
 * Utility class that helps to resolve country info from ip address
 * Used maxmind geoip database and api http://maxmind.github.io/GeoIP2-java/
 */
public class GeoIP {

    private static final Logger logger = Logger.getLogger(GeoIP.class);

    public static class CountryInfo {
        private final String iso;
        private final String name;

        private CountryInfo(String name, String iso) {
            this.name = name;
            this.iso = iso;
        }

        public String iso() {
            return iso;
        }

        public String name() {
            return name;
        }
    }

    private static DatabaseReader reader;

    public static void open(String db) {
        if (reader == null) {
            try {
                reader = new DatabaseReader.Builder(new File(db)).build();
            } catch (IOException e) {
                logger.error("failed to open database reader for " + db);
            }
        }
    }

    public static void close() {
        if (reader != null) {
            try {
                reader.close();
            } catch (IOException e) {
                logger.error("failed to close database reader");
            }
        }
    }

    public CountryInfo resolveCountryInfo(String ip) {
        if (reader == null) {
            throw new IllegalStateException("reader closed");
        }

        String iso = null;
        String name = null;
        try {
            InetAddress ipAddress = InetAddress.getByName(ip);
            CountryResponse response = reader.country(ipAddress);
            Country country = response.getCountry();
            name = country.getName();
            iso = country.getIsoCode();
        } catch (Exception e) {
            logger.warn("failed to resolve country info for ip " + ip);
        }

        return new CountryInfo(name, iso);
    }

}
