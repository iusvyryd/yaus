package iusvyryd.yaus.web.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public abstract class HttpFilter implements Filter {

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException
    {
        doHttpFilter((HttpServletRequest)servletRequest,
                (HttpServletResponse)servletResponse,
                filterChain);
    }

    public abstract void doHttpFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain filterChain)
            throws IOException, ServletException;

}
