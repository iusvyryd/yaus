package iusvyryd.yaus.web.filter;

import org.apache.log4j.Logger;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter(value = "/*", initParams = {
        @WebInitParam(name = "encoding", value = "UTF-8"),
        @WebInitParam(name = "exclude", value = "/static/::/webjars/")
})
public class EncodingFilter extends HttpFilter {

    private static final Logger logger = Logger.getLogger(EncodingFilter.class);

    private String encoding;
    private String[] exclude;

    @Override
    public void doHttpFilter(HttpServletRequest req, HttpServletResponse resp, FilterChain filterChain)
            throws IOException, ServletException
    {
        String path = req.getRequestURI();
        if (!isExcluded(path)) {
            logger.info("set encoding for:" + path);

            if (encoding != null) {
                req.setCharacterEncoding(encoding);
                resp.setCharacterEncoding(encoding);
            }
        }

        filterChain.doFilter(req, resp);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        encoding = filterConfig.getInitParameter("encoding");
        String excludeParam = filterConfig.getInitParameter("exclude");
        exclude = excludeParam.split("::");
    }

    @Override
    public void destroy() {
    }

    private boolean isExcluded(String path) {
        for (String ex : exclude) {
            if (path.indexOf(ex) != -1) {
                return true;
            }
        }
        return false;
    }
}
