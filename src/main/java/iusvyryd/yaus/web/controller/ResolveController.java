package iusvyryd.yaus.web.controller;

import org.apache.log4j.Logger;
import iusvyryd.yaus.model.Click;
import iusvyryd.yaus.model.Url;
import iusvyryd.yaus.service.Service;
import iusvyryd.yaus.exception.YausException;
import iusvyryd.yaus.service.ServiceFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@WebServlet(value = "/u/*", initParams = {
        @WebInitParam(name = "thread-pool-size", value = "100")
})
public class ResolveController extends HttpServlet {

    private static final Logger logger = Logger.getLogger(ResolveController.class);

    private Service service;
    private String serviceUrl;
    private ExecutorService statExecutor;

    @Override
    public void init(ServletConfig config) throws ServletException {
        serviceUrl = config.getServletContext().getInitParameter("service-main")
                .replace("{{prefix}}", (String)config.getServletContext().getAttribute("prefix"));
        logger.debug("main service url: " + serviceUrl);

        int size = Integer.valueOf(config.getInitParameter("thread-pool-size"));
        statExecutor = Executors.newFixedThreadPool(size);
        service = ServiceFactory.createService();
        logger.debug("click info collector service started for " + size + " threads");
    }

    @Override
    public void destroy() {
        statExecutor.shutdown();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String path = req.getRequestURI();
        String shortId = path.substring(path.lastIndexOf("/") + 1);

        if (shortId.length() < 10) {
            try {
                Url url = service.resolveLongUrl(shortId);
                if (url != null) {
                    resp.sendRedirect(resp.encodeRedirectURL(url.getLongUrl()));

                    Click stat = new Click(url);

                    String referer = req.getHeader("Referer");
                    String userAgent = req.getHeader("User-Agent");
                    String ip = req.getHeader("X-FORWARDED-FOR");
                    if (ip == null) {
                        ip = req.getRemoteAddr();
                    }

                    logger.debug("start collecting click info for ip=" + ip + " referer=" + referer + " ua=" + userAgent);
                    startPickClickInfo(stat, ip, userAgent, referer);
                    return;
                }
            }
            catch (YausException e) {
                logger.error("error while resolving \"" + shortId + "\"", e);
            }
        }
        else {
            logger.warn("short url unknown format \"" + shortId + "\"");
        }

        resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
        resp.sendRedirect(resp.encodeRedirectURL(serviceUrl + "/#/404/" +
                req.getRequestURL().toString()));
    }

    private void startPickClickInfo(final Click click, final String ip, final String userAgent, final String referer) {
        statExecutor.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    service.processNewClick(click, ip, userAgent, referer);
                } catch (YausException e) {
                }
            }
        });
    }
}
