package iusvyryd.yaus.web.controller;

import org.apache.log4j.Logger;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Simple Redirect Controller
 * Redirects to url specified in "redirect-url" init parameter
 */
public class RedirectController extends HttpServlet {

    private static final Logger logger = Logger.getLogger(RedirectController.class);
    private String redirectUrl;

    @Override
    public void init(ServletConfig config) throws ServletException {
        redirectUrl = config.getInitParameter("redirect-url")
            .replace("{{prefix}}", (String)config.getServletContext().getAttribute("prefix"));
        logger.debug("redirect controller to " + redirectUrl);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String redirect = redirectUrl;
        if (redirect.indexOf("{{short}}") != -1) {
            String path = req.getRequestURI();
            String shortId = path.substring(path.lastIndexOf("/") + 1);
            redirect = redirect.replace("{{short}}", shortId);
        }
        resp.sendRedirect(resp.encodeRedirectURL(redirect));
        logger.debug("redirecting to " + redirect);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
