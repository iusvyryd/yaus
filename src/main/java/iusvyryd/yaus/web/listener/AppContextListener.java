package iusvyryd.yaus.web.listener;

import org.apache.log4j.Logger;
import iusvyryd.yaus.dao.jpa.Emf;
import iusvyryd.yaus.util.geoip.GeoIP;

import javax.persistence.EntityManagerFactory;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class AppContextListener implements ServletContextListener {

    private static final Logger logger = Logger.getLogger(AppContextListener.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        ServletContext context = servletContextEvent.getServletContext();

        // jpa
        // force loading emf now
        logger.debug("initializing jpa entity manager factory");
        Emf.init("yausPU");

        // geoip
        logger.debug("initializing geoip database");
        GeoIP.open(getClass().getClassLoader().getResource("geoip/GeoLite2-Country.mmdb").getFile());

        // init prefix
        String proto = context.getInitParameter("protocol");
        String host = context.getInitParameter("host");
        context.setAttribute("prefix", String.format("%s://%s", proto, host));
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        logger.debug("closing geoip database");
        GeoIP.close();

        logger.debug("closing jpa entity manager factory");
        EntityManagerFactory emf = Emf.get();
        if (emf != null && emf.isOpen()) {
            emf.close();
        }
    }
}
