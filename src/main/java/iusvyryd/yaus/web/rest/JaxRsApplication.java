package iusvyryd.yaus.web.rest;

import iusvyryd.yaus.exception.YausExceptionMapper;
import iusvyryd.yaus.service.Service;
import iusvyryd.yaus.service.ServiceFactory;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.jackson1.Jackson1Feature;
import org.glassfish.jersey.server.ResourceConfig;

public class JaxRsApplication extends ResourceConfig {

    public JaxRsApplication() {
        register(Api.class);
        register(YausExceptionMapper.class);
        register(Jackson1Feature.class);
        register(new AbstractBinder() {
            @Override
            protected void configure() {
                bindFactory(ServiceFactory.class).to(Service.class);
            }
        });
    }

}
