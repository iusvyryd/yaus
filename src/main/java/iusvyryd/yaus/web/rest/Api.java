package iusvyryd.yaus.web.rest;

import iusvyryd.yaus.exception.YausException;
import iusvyryd.yaus.model.Statistics;
import iusvyryd.yaus.model.Url;
import iusvyryd.yaus.service.Service;
import org.apache.log4j.Logger;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/")
public class Api {

    private static final Logger logger = Logger.getLogger(Api.class);

    @Context
    private ServletContext context;
    @Inject
    private Service service;

    private String shortPtr;
    private String statsPtr;

    private void readInitParams() {
        shortPtr = context.getInitParameter("service-short")
                .replace("{{prefix}}", (String) context.getAttribute("prefix"));
        statsPtr = context.getInitParameter("service-stats")
                .replace("{{prefix}}", (String) context.getAttribute("prefix"));
    }

    @POST
    @Path("doshort")
    @Consumes("application/json")
    @Produces("application/json")
    public Response doShort(Url url) throws YausException {
        readInitParams();

        url = service.processNewLongUrl(url);
        url.setShortUrl(shortPtr.replace("{{short}}", url.getShortId()));
        url.setStatsUrl(statsPtr.replace("{{short}}", url.getShortId()));

        logger.debug("doShort: new short url created. short=" + url.getShortId() +
                " long=" + url.getLongUrl());

        return Response.status(Response.Status.OK).entity(url).build();
    }

    @GET
    @Path("getstats/{id}")
    @Produces("application/json")
    public Response getStats(@PathParam("id") String shortId) throws YausException {
        readInitParams();

        Url url = service.resolveLongUrl(shortId);
        url.setShortUrl(shortPtr.replace("{{short}}", shortId));

        // prepare statistics
        Statistics stat = service.getFullStatistics(url);
        return Response.status(Response.Status.OK).entity(stat).build();
    }

    @GET
    @Path("all")
    @Produces("application/json")
    public Response getAll() throws YausException {
        readInitParams();

        List<Url> urls = service.getAllUrls();
        for (Url u : urls) {
            u.setShortUrl(u.getShortId());
            u.setShortUrl(shortPtr.replace("{{short}}", u.getShortId()));
            u.setStatsUrl(statsPtr.replace("{{short}}", u.getShortId()));
        }
        return Response.status(Response.Status.OK).entity(urls).build();
    }
}
