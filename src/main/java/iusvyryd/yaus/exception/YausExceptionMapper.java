package iusvyryd.yaus.exception;

import org.apache.log4j.Logger;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

/**
 * Implementation of jaxrs ExceptionMapper for {@link iusvyryd.yaus.exception.YausException}
 */
public class YausExceptionMapper implements ExceptionMapper<YausException> {

    private static final Logger logger = Logger.getLogger(YausExceptionMapper.class);

    @Override
    public Response toResponse(YausException e) {
        logger.error("Handled exception: " + e.getMessage(), e.getCause());

        return Response.status(e.getStatus())
                .entity(new ErrorResponse(e.getMessage()))
                .type(MediaType.APPLICATION_JSON)
                .build();
    }

}
