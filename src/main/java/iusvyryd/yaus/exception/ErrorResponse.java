package iusvyryd.yaus.exception;

/**
 * Simple entity for holding <code>YausException</code> message
 * and serialization to JSON
 */
public class ErrorResponse {

    private String message;

    public ErrorResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "ErrorResponse{" +
                "message='" + message + '\'' +
                '}';
    }
}
