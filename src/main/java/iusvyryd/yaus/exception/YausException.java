package iusvyryd.yaus.exception;

import javax.ws.rs.core.Response;

/**
 * Common exception that can throws by service
 */
public class YausException extends Exception {

    private Response.Status status;

    public YausException(String message) {
        this(message, Response.Status.INTERNAL_SERVER_ERROR);
    }

    public YausException(String message, Response.Status status) {
        super(message);
        this.status = status;
    }

    public YausException(String message, Response.Status status, Throwable cause) {
        super(message, cause);
        this.status = status;
    }

    public Response.Status getStatus() {
        return status;
    }
}
