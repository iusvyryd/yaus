package iusvyryd.yaus.dao;

import iusvyryd.yaus.model.Click;
import iusvyryd.yaus.model.Statistics;
import iusvyryd.yaus.model.Url;

/**
 * Data access object class for <code>Click</code> domain object.
 */
public interface ClickDao extends AutoCloseable {

    /**
     * Save <code>Click</code> to the data store
     *
     * @param click the <code>Click</code> to save
     * @return returns saved <code>Click</code>
     * @throws java.lang.IllegalStateException - if dao was closed
     */
    public Click save(Click click);

    /**
     * Retrieve grouped statistics about clicks on <code>Url</code>
     *
     * @param url the <code>Url</code> to get statistics about
     * @return <code>Statistics</code> object
     * @throws java.lang.IllegalStateException - if dao was closed
     */
    public Statistics totalStatisticsReport(Url url);
}
