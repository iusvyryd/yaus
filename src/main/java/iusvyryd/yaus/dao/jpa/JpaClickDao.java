package iusvyryd.yaus.dao.jpa;

import iusvyryd.yaus.dao.ClickDao;
import iusvyryd.yaus.model.*;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * JPA implementation of {@link iusvyryd.yaus.dao.ClickDao}
 */
public class JpaClickDao implements ClickDao {

    private final EntityManager em;

    public JpaClickDao() {
        this(Emf.get().createEntityManager());
    }

    public JpaClickDao(EntityManager em) {
        this.em = em;
    }

    @Override
    public Click save(Click click) {
        checkEmState();

        if (click.getId() == null) {
            em.getTransaction().begin();
            em.persist(click);
            em.getTransaction().commit();
        }
        return click;
    }

    @Override
    public Statistics totalStatisticsReport(Url url) {
        checkEmState();

        Statistics stat = new Statistics(url);

        CriteriaBuilder cb = em.getCriteriaBuilder();
        // total count
        stat.setTotalCount(getTotalCount(cb, url));
        // grouped by browsers
        List<Object[]> broList = getGroupedByBrowsers(cb, url);
        if (broList != null) {
            Map<String, Long> broMap = new LinkedHashMap<>(broList.size());
            for (Object[] o : broList) {
                broMap.put(o[0] == null ? "undefined" : (String)o[0], (Long)o[1]);
            }
            stat.setGroupedByBrowser(broMap);
        }

        // grouped by referer
        List<Object[]> refList = getGroupedByReferer(cb, url);
        if (refList != null) {
            Map<String, Long> refMap = new LinkedHashMap<>(refList.size());
            for (Object[] o : refList) {
                refMap.put(o[0] == null ? "undefined" : (String)o[0], (Long)o[1]);
            }
            stat.setGroupedByReferer(refMap);
        }

        // grouped by platform
        List<Object[]> plList = getGroupedByPlatform(cb, url);
        if (plList != null) {
            Map<String, Long> plMap = new LinkedHashMap<>(plList.size());
            for (Object[] o : plList) {
                plMap.put(o[0] == null ? "undefined" : (String)o[0], (Long)o[1]);
            }
            stat.setGroupedByPlatform(plMap);
        }

        // grouped by location
        List<Object[]> locList = getGroupedByLocation(cb, url);
        if (locList != null) {
            Map<Country, Long> locMap = new LinkedHashMap<>(locList.size());
            for (Object[] o : locList) {
                locMap.put(o[0] == null
                                ? new Country("undefined", "undefined")
                                : new Country((String)o[0], (String)o[1]),
                        (Long)o[2]);
            }
            stat.setGroupedByCountry(locMap);
        }

        // grouped by date
        List<Object[]> dtList = getGroupedByDate(cb, url);
        if (dtList != null) {
            Map<Date, Long> dtMap = new LinkedHashMap<>(dtList.size());
            for (Object[] o : dtList) {
                dtMap.put((Date)o[0], (Long)o[1]);
            }
            stat.setGroupedByDate(dtMap);
        }

        return stat;
    }

    private long getTotalCount(CriteriaBuilder cb, Url url) {
        CriteriaQuery<Long> cq = cb.createQuery(Long.class);
        Root<Click> root = cq.from(Click.class);
        Join<Click, Url> join =  root.join("url", JoinType.INNER);
        cq.select(cb.count(root));
        cq.where(cb.equal(join.get(Url_.id), url.getId()));
        return em.createQuery(cq).getSingleResult();
    }

    private List<Object[]> getGroupedByBrowsers(CriteriaBuilder cb, Url url) {
        CriteriaQuery<Object[]> cq = cb.createQuery(Object[].class);
        Root<Click> root = cq.from(Click.class);
        Join<Click, Url> join =  root.join("url", JoinType.INNER);
        cq.multiselect(root.get(Click_.browser), cb.count(root));
        cq.where(cb.equal(join.get(Url_.id), url.getId()))
                .groupBy(root.get(Click_.browser))
                .orderBy(cb.desc(cb.count(root)));
        return em.createQuery(cq).getResultList();
    }

    private List<Object[]> getGroupedByReferer(CriteriaBuilder cb, Url url) {
        CriteriaQuery<Object[]> cq = cb.createQuery(Object[].class);
        Root<Click> root = cq.from(Click.class);
        Join<Click, Url> join =  root.join("url", JoinType.INNER);
        cq.multiselect(root.get(Click_.referer), cb.count(root));
        cq.where(cb.equal(join.get(Url_.id), url.getId()))
                .groupBy(root.get(Click_.referer))
                .orderBy(cb.desc(cb.count(root)));
        return em.createQuery(cq).getResultList();
    }

    private List<Object[]> getGroupedByPlatform(CriteriaBuilder cb, Url url) {
        CriteriaQuery<Object[]> cq = cb.createQuery(Object[].class);
        Root<Click> root = cq.from(Click.class);
        Join<Click, Url> join =  root.join("url", JoinType.INNER);
        cq.multiselect(root.get(Click_.platform), cb.count(root));
        cq.where(cb.equal(join.get(Url_.id), url.getId()))
                .groupBy(root.get(Click_.platform))
                .orderBy(cb.desc(cb.count(root)));
        return em.createQuery(cq).getResultList();
    }

    private List<Object[]> getGroupedByLocation(CriteriaBuilder cb, Url url) {
        CriteriaQuery<Object[]> cq = cb.createQuery(Object[].class);
        Root<Click> root = cq.from(Click.class);
        Join<Click, Url> join =  root.join("url", JoinType.INNER);
        cq.multiselect(root.get(Click_.country).get(Country_.name),
                root.get(Click_.country).get(Country_.iso),
                cb.count(root));
        cq.where(cb.equal(join.get(Url_.id), url.getId()))
                .groupBy(root.get(Click_.country))
                .orderBy(cb.desc(cb.count(root)));
        return em.createQuery(cq).getResultList();
    }

    private List<Object[]> getGroupedByDate(CriteriaBuilder cb, Url url) {
        CriteriaQuery<Object[]> cq = cb.createQuery(Object[].class);
        Root<Click> root = cq.from(Click.class);
        Join<Click, Url> join =  root.join("url", JoinType.INNER);
        cq.multiselect(root.get(Click_.creationDate), cb.count(root));
        cq.where(cb.equal(join.get(Url_.id), url.getId()))
                .groupBy(root.get(Click_.creationDate))
                .orderBy(cb.asc(root.get(Click_.creationDate)));
        return em.createQuery(cq).getResultList();
    }

    @Override
    public void close() {
        if (em.isOpen()) {
            em.close();
        }
    }

    private void checkEmState() {
        if (!em.isOpen()) {
            throw new IllegalStateException("Trying to use closed Entity Manager");
        }
    }

}
