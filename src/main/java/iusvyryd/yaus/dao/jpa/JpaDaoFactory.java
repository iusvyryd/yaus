package iusvyryd.yaus.dao.jpa;

import iusvyryd.yaus.dao.DaoFactory;
import iusvyryd.yaus.dao.ClickDao;
import iusvyryd.yaus.dao.UrlDao;

/**
 * Concrete JPA DAO factory
 * Used to create JPA DAO objects
 * extends {@link DaoFactory}
 */
public final class JpaDaoFactory extends DaoFactory {
    @Override
    public UrlDao createUrlDao() {
        return new JpaUrlDao();
    }

    @Override
    public ClickDao createClickDao() {
        return new JpaClickDao();
    }
}
