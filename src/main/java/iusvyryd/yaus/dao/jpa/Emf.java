package iusvyryd.yaus.dao.jpa;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Entity Manager factory singleton
 */
public final class Emf {
    private static EntityManagerFactory instance;

    private Emf(String su) {
        instance = Persistence.createEntityManagerFactory(su);
    }

    /**
     * Return EntityManagerFactory instance
     *
     * @return EntityManagerFactory instance
     */
    public static EntityManagerFactory get() {
        return instance;
    }

    /**
     * Initialize EntityManagerFactory with persistence unit name
     *
     * @param su persistence unit name
     */
    public static void init(String su) {
        if (instance == null) {
            new Emf(su);
        }
    }
}
