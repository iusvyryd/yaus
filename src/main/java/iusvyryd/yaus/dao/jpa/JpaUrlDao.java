package iusvyryd.yaus.dao.jpa;

import iusvyryd.yaus.dao.UrlDao;
import iusvyryd.yaus.model.Url;
import iusvyryd.yaus.model.Url_;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * JPA implementation of {@link UrlDao}
 */
public class JpaUrlDao implements UrlDao {

    private final EntityManager em;

    public JpaUrlDao() {
        this(Emf.get().createEntityManager());
    }

    public JpaUrlDao(EntityManager em) {
        this.em = em;
    }

    @Override
    public Url save(Url url) {
        checkEmState();

        if (url.getId() == null) {
            em.getTransaction().begin();
            em.persist(url);
            em.getTransaction().commit();
        }
        return url;
    }

    @Override
    public void remove(Url url) {
        checkEmState();

        if (url.getId() != null) {
            em.getTransaction().begin();
            em.remove(url);
            em.getTransaction().commit();
        }
    }

    @Override
    public List<Url> getAll() {
        checkEmState();

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Url> cq = cb.createQuery(Url.class);
        Root<Url> root = cq.from(Url.class);
        cq.select(root);
        cq.orderBy(cb.desc(root.get(Url_.creationTimestamp)));

        return em.createQuery(cq).getResultList();
    }

    @Override
    public Url findByLongUrlHash(int hash) {
        checkEmState();

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Url> cq = cb.createQuery(Url.class);
        Root<Url> root = cq.from(Url.class);

        ParameterExpression<Integer> param = cb.parameter(Integer.class);
        cq = cq.select(root).where(cb.equal(root.get("longUrlHash"), param));

        TypedQuery<Url> query = em.createQuery(cq);
        query.setParameter(param, hash);

        return query.getSingleResult();
    }

    @Override
    public Url findByShortId(String shortId) {
        checkEmState();

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Url> cq = cb.createQuery(Url.class);
        Root<Url> root = cq.from(Url.class);
        ParameterExpression<String> param = cb.parameter(String.class);
        cq = cq.select(root).where(cb.equal(root.get("shortId"), param));

        TypedQuery<Url> query = em.createQuery(cq);
        query.setParameter(param, shortId);
        return query.getSingleResult();
    }

    @Override
    public void close() {
        if (em.isOpen()) {
            em.close();
        }
    }

    private void checkEmState() {
        if (!em.isOpen()) {
            throw new IllegalStateException("Trying to use closed Entity Manager");
        }
    }
}
