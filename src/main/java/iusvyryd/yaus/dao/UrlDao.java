package iusvyryd.yaus.dao;

import iusvyryd.yaus.model.Url;

import java.util.List;

/**
 * Data access object class for <code>Url</code> domain object.
 */
public interface UrlDao extends AutoCloseable {

    /**
     * Save <code>Url</code> to the data store
     *
     * @param url the <code>Url</code> to save
     * @return returns saved <code>Url</code>
     * @throws java.lang.IllegalStateException - if dao was closed
     */
    Url save(Url url);

    /**
     * Remove <code>Url</code> from the data store
     * @param url the <code>Url</code> to remove
     * @throws java.lang.IllegalStateException - if dao was closed
     */
    void remove(Url url);

    /**
     * Retrieve all <code>Url</code> from the data store
     * @return list of <code>Url</code> saved in the data store
     * @throws java.lang.IllegalStateException - if dao was closed
     */
    List<Url> getAll();

    /**
     * Retrieve <code>Url</code> from the data store by long url hash
     *
     * @param hash integer hash value
     * @return the <code>Url</code> if found
     * @throws javax.persistence.NoResultException - if there is no result,
     * @throws javax.persistence.NonUniqueResultException - if more than one result
     * @throws java.lang.IllegalStateException - if dao was closed
     */
    Url findByLongUrlHash(int hash);

    /**
     * Retrieve <code>Url</code> from the data store by lshort id
     *
     * @param shortId string short id
     * @return the <code>Url</code> if found
     * @throws javax.persistence.NoResultException - if there is no result,
     * @throws javax.persistence.NonUniqueResultException - if more than one result
     * @throws java.lang.IllegalStateException - if dao was closed
     */
    Url findByShortId(String shortId);
}
