package iusvyryd.yaus.dao;

import iusvyryd.yaus.dao.jpa.JpaDaoFactory;

/**
 * Abstract DAO factory.
 * Serves as base class for concrete dao factories
 */
public abstract class DaoFactory {

    public abstract UrlDao createUrlDao();

    public abstract ClickDao createClickDao();

    /**
     * Create concrete DAO factory
     *
     * @return concrete factory
     */
    public static DaoFactory createDaoFactory() {
        return new JpaDaoFactory();
    }

}
