# Yet Another Url Shortener

Simple Url shortener service.
It helps to shorten long urls and collects statistics about short url usage.
For each short url provides statistics and you can find out full grouped information about url usage.

Written as a test project, to try some new technologies for me.

###Architecture
Presentation layer: AngularJS application

Web: Servlets

Business: JAX-RS (Jersey)

DataAccess: JPA (Hibernate)

DataStore: DB (MySql)

###Requirement
Maven 3 - to build, Apache Tomcat - to deploy on.

###Used technologies
* Servlet, JPA2 (with Hibernate JPA provider), JAX-RS(jersey + jackson)
* MySql, H2DB (in-memory database for tests)
* Log4j
* JUnit, Mockito, PowerMockito, Jersey Test Framework
* AngularJS, HighCharts, Bootstrap
* MaxMind GeoipLite, Gson

####TODO
* try spring